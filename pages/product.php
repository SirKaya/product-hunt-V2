<?php
    include '../includes/connexion.php';
    include '../includes/lancement_session.php';

    $produitQuery = $bdd->query('SELECT * FROM Produit WHERE id='.$_GET["id_produit"]);

    $produit = $produitQuery->fetch();
?>

    <!doctype html>

    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
            crossorigin="anonymous">
        <link rel="stylesheet" href="../assets/css/style.css" />
        <link href="https://fonts.googleapis.com/css?family=EB+Garamond|Indie+Flower" rel="stylesheet">
        <title>Product-hunt</title>
    </head>

    <body>
        <!-- Include Nav bar -->
        <div class="sticky-top">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a class="navbar-brand-logo" href="../index.php">
                        <img src="../assets/img/logo.jpg" width="50" height="50" alt="coccinelle" style="border-radius: 50%;">
                    </a>
                    <form class="form-inline mr-auto mt-2 mt-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Recherche" aria-label="Search">
                        <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">🧐</button>
                    </form>

                    <div class="btn-group nav-link navbar-nav my-2 my-lg-0" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            Explorer
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <a class="dropdown-item" href="fastfood.php">Fastfood</a>
                            <a class="dropdown-item" href="automobile.php">Automobile</a>
                            <a class="dropdown-item" href="sports.php">Sports</a>
                        </div>
            </nav>
            </div>
            <!-- Fin nav bar -->
            <section>
                <div class="jumbotron col-8 offset-2 bg-white fond">
                    <div class="container">
                        <div class="media">
                            <img class="mr-3" src="../<?php echo $produit['logo']; ?>" width="100" height="120" alt="Logo">
                            <div class="media-body-center">
                                <h1 class="mt-0">
                                    <?php echo $produit['nom_produit']; ?>
                                </h1>
                                <h5>
                                    <?php echo $produit['mini_description']; ?>
                                </h5>
                            </div>
                            <div class="col text-right">
                                <!--boutons-->
                                <button type="button" class="btn btn-secondary">
                                    <?php include("../includes/affichage_like.php"); ?>
                                    <!-- 👍<span class="badge badge-pill badge-light">0</span> -->
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </section>




            <section>
                <div class="jumbotron col-8 offset-2 bg-white fond">
                    <div class="container">

                        <!--description détaillée-->
                        <p class="lead">
                            <?php echo $produit['description_detaille']; ?>
                        </p>
                    </div>
                </div>
            </section>




            <!--formulaire pour commentaire-->
            <div id="actu" class='jumbotron col-8 offset-2 bg-white fond'>
                <div class='container'>
                    <h4 class='text-center'>Commentaires:</h4>
                        <div>
                            <!--Affichage commentaire-->
                            <?php include '../includes/commentaire_affichage.php' ?>
                        </div>
                        <h4 class='text-center'>Tapez votre commentaires:</h4> 
                    <form method='POST' action="../includes/commentaire_post.php" onsubmit="storeMessage(event, this)">
                        <input name='id_produit' type='hidden' value="<?php echo $produit['id']; ?>">
                        <div class='form-group'>
                            <label for='pseudo'>Pseudo</label>
                            <input name="pseudo" type='text' class='form-control bg-light' id='pseudo' aria-describedby='pseudoHelp' placeholder='Entrer votre pseudo'>
                            <small id='pseudoHelp' class='form-text text-muted'>Vortre pseudo ne doit pas dépasser plus de 50 caractère.</small>
                        </div>
                        <div class='form-group'>
                            <label for='message'>Message</label>
                            <input name="message" type='text' class='form-control bg-light' id='message' placeholder='Entrer votre commentaire'>
                        </div>
                        <button id="btn2" type='submit' class='btn btn-primary' name="submit_message">Valider</button>
                    </form>
                </div>
            </div>


            <!-- Optional JavaScript -->
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
                crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
                crossorigin="anonymous"></script>
            <script src="../js/likes.js"></script>
    </body>

    </html>