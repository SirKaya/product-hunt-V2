<?php 
    //On récupére tout le contenu de la table produit
    $reponse = $bdd->query('SELECT * FROM Produit WHERE description_detaille LIKE "%'.$_POST['recherche'].'%" OR mini_description LIKE "%'.$_POST['recherche'].'%"');

    //$likes = $bdd->query('SELECT id_produit, COUNT(id_produit) FROM likes GROUP BY id_produit');
    //On affiche chaque entrée une à une
    while ($produit = $reponse->fetch())
    {
        ?>

<div class="list-group-item list-group-item-action" data-toggle="modal" data-target="#exampleModalCenter">

  <div class="container contZindex">

    <div class="row ">

      <div class="col-3 text-center logZindex">
        <img src="<?php echo $produit['logo']; ?>" width="100" height="100" alt="logo">
        <!--logo-->
      </div>
      <div class="col-6">
        <div class="list-group list-group-action flex-column align-items-start descZindex">
          <a class="lien" href="<?php echo $produit['lien_site']; ?>">
            <h5 class="mb-1">
              <?php echo $produit['nom_produit']; ?>
            </h5>
            <!--description produit-->
            <small class="text-muted">
              <?php echo $produit['mini_description']; ?>
            </small>
          </a>
        </div>
      </div>

      <div class="col-3 align-self-end text-right btnZindex">
        <!--boutons-->
        <button id="" type="button" class="btn btn-primary like" data-id-produit="<?php echo $produit['id']; ?>">

          <?php include("affichage_like.php"); ?>
        </button>
        <a class="btn btn-primary commentaire" href="pages/product.php?id_produit=<?php echo $produit['id']; ?>">
          💭
        </a>
      </div>

    </div>

  </div>

</div>
<?php
    }

    $reponse->closeCursor(); //Termine le traitement de la requete
    ?>

  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>