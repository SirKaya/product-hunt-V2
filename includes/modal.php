<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="Modal<?php echo $produit['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-body">


                <section>
                    <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                            <div class="media">
                                <img class="mr-3" src="<?php echo $produit['logo']; ?>" width="100" height="120" alt="Logo">
                                <div class="media-body">
                                    <h1 class="mt-0">
                                        <?php echo $produit['nom_produit']; ?>
                                    </h1>
                                    <p>
                                        <?php echo $produit['mini_description']; ?>
                                    </p>
                                </div>
                                <div class="col text-right">
                                    <!--boutons-->
                                    <button type="button" class="btn btn-secondary">
                                        <?php include("affichage_like.php"); ?>
                                        <!-- 👍<span class="badge badge-pill badge-light">0</span> -->
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>




                <section>
                    <div class="jumbotron jumbotron-fluid">
                        <div class="container">

                            <!--description détaillée-->
                            <p class="lead">
                                <?php echo $produit['description_detaille']; ?>
                            </p>
                        </div>
                    </div>
                </section>

                <!--Affichage commentaire-->




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>