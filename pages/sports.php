<?php 
  include '../includes/connexion.php';
  include '../includes/lancement_session.php';
  
?>

<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
  <link rel="stylesheet" href="../assets/css/style.css" />
  <link href="https://fonts.googleapis.com/css?family=EB+Garamond|Indie+Flower" rel="stylesheet">
  <title>Product-hunt</title>
</head>

<body>

  <!--création navbar -->
  <div class="sticky-top">
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand-logo mr-auto mt-2 mt-lg-0" href="../index.php">
          <img src="../assets/img/logo.jpg" width="50" height="50" alt="coccinelle" style="border-radius: 50%;">
        </a>
        <form class="form-inline mr-2 mt-2 mt-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Recherche" aria-label="Search">
          <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">🧐</button>
        </form>

        <div class="btn-group nav-link navbar-nav my-2 my-lg-0" role="group">
          <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            Explorer
          </button>
          <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <a class="dropdown-item" href="fastfood.php">Fastfood</a>
            <a class="dropdown-item" href="automobile.php">Automobile</a>
            <a class="dropdown-item" href="sports.php">Sports</a>
          </div>

    </nav>
    </div>
    <!--header-->
    <div class="container fondHeader">
      <div class="row">
        <div class="col align-self-center">
          <h2>
            <strong>Bienvenue sur notre magnifique site!</stong>
          </h2>
          <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean hendrerit sapien odio, id vestibulum felis ultrices
            vitae. Ut sed feugiat metus. Etiam non nibh dolor. Aliquam nec dui vel erat posuere, blablablabla!!
          </h5>
        </div>
        <div class="col text-center">
          <img src="../assets/img/logo.jpg" width="250" height="250" alt="coccinelle" style="border-radius: 50%;">
        </div>
      </div>

      <!--centre de page liste des produits-->
      <div class="list-group">
        <div class="list-group-item list-group-item text-white bg-dark">
          <h3>Sports</h3>
          <!--titre-->
        </div>


        <?php 
    //On récupére tout le contenu de la table produit
    $reponse = $bdd->query('SELECT * FROM Produit WHERE id_categorie = "3"');

    //$likes = $bdd->query('SELECT id_produit, COUNT(id_produit) FROM likes GROUP BY id_produit');
    //On affiche chaque entrée une à une
    while ($produit = $reponse->fetch())
    {
        ?>

        <div class="list-group-item list-group-item-action" data-toggle="modal" data-target="#exampleModalCenter">

          <div class="container contZindex">

            <div class="row ">

              <div class="col-3 text-center logZindex">
                <img src="../<?php echo $produit['logo']; ?>" width="100" height="100" alt="logo">
                <!--logo-->
              </div>
              <div class="col-6">
                <div class="list-group list-group-action flex-column align-items-start descZindex">
                  <a class="lien" href="<?php echo $produit['lien_site']; ?>">
                    <h4 class="mb-1">
                      <?php echo $produit['nom_produit']; ?>
                    </h4>
                    <!--description produit-->
                    <p class="text-muted">
                      <?php echo $produit['mini_description']; ?>
                    </p>
                  </a>
                </div>
              </div>

              <div class="col-3 align-self-end text-right btnZindex">
                <!--boutons-->
                <button id="" type="button" class="btn btn-secondary like" data-id-produit="<?php echo $produit['id']; ?>">

                  <?php include("../includes/affichage_like.php"); ?>
                </button>
                <a class="btn btn-secondary commentaire" href="../pages/product.php?id_produit=<?php echo $produit['id']; ?>">
                  💭
                </a>
              </div>

            </div>

          </div>

        </div>
        <?php
    }

    $reponse->closeCursor(); //Termine le traitement de la requete
    ?>

          <!-- Modal -->
          <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  ...
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
              </div>
            </div>
          </div>








          <!-- jQuery first, then Popper.js, then Bootstrap JS -->

          <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
          <script src="../js/likes.js"></script>
</body>

</html>