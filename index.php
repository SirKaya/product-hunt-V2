<?php 
  include 'includes/connexion.php';
  include 'includes/lancement_session.php';
?>

<!doctype html>
<html lang="fr">

<?php 
    include 'includes/head.php'
?>

<body>

  <!--création navbar -->
  <div class="sticky-top">
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand mr-auto mt-2 mt-lg-0" href="#">
          <img src="assets/img/logo.jpg" width="50" height="50" alt="coccinelle" style="border-radius: 50%;">
        </a>
        <form method="POST" action="index_recherche.php" class="form-inline mr-2 mt-2 mt-lg-0">
          <input name="recherche" class="form-control mr-sm-2" type="search" placeholder="Recherche" aria-label="Search">
          <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">🧐</button>
        </form>

        <div class="btn-group nav-link navbar-nav my-2 my-lg-0" role="group">
          <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            Explorer
          </button>
          <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <a class="dropdown-item" href="pages/fastfood.php">Fastfood</a>
            <a class="dropdown-item" href="pages/automobile.php">Automobile</a>
            <a class="dropdown-item" href="pages/sports.php">Sports</a>
          </div>

    </nav>
    </div>
    <!--header-->
    <div class="container fondHeader p-3 mb-2 bg-light text-dark">
      <div class="row">
        <div class="col align-self-center">
          <h2>
            <strong>Bienvenue sur notre magnifique site!</strong>
          </h2>
          <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
             Aenean hendrerit sapien odio, id vestibulum felis ultrices vitae.
              Ut sed feugiat metus. Etiam non nibh dolor. Aliquam nec dui vel erat posuere,  
              blablablabla!!</h5>
        </div>
        <div class="col text-center">
          <img src="assets/img/logo.jpg" width="250" height="250" alt="coccinelle" style="border-radius: 50%;">
        </div>
      </div>

      <!--centre de page liste des produits-->
      <div class="list-group ">
        <div class="list-group-item list-group-item text-white bg-dark">
          <h3>Populaire en ce moment</h3>
          <!--titre-->
        </div>


        <?php
          include 'includes/affichage_product.php'
      ?>








          <!-- jQuery first, then Popper.js, then Bootstrap JS -->

          <?php include 'includes/script.php'   ?>
</body>

</html>